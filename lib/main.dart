import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_example/splash.dart';
// import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'bloc_example/counter_bloc.dart';
import 'bloc_example/counter_event.dart';
import 'riverpod_example/counter_controller.dart';

void main() {
  runApp(const MyApp());
}

class BlocCounterExample extends StatefulWidget {
  @override
  _BlocCounterExampleState createState() => _BlocCounterExampleState();
}

class _BlocCounterExampleState extends State<BlocCounterExample> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocProvider(
          create: (_) => CounterBloc(),
          child: CounterPage(),
        ),
      ],
    );
  }
}

class CounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: BlocBuilder<CounterBloc, int>(
            builder: (context, count) {
              return Text('$count',
                  style: Theme.of(context).textTheme.displayMedium);
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton(
                onPressed: () => context.read<CounterBloc>().add(Increment()),
                icon: const Icon(Icons.add)),
            IconButton(
                onPressed: () => context.read<CounterBloc>().add(Decrement()),
                icon: const Icon(Icons.remove)),
          ],
        )
      ],
    );
  }
}

class RiverpodCounterExample extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final counter = ref.watch(counterProvider);
    return Column(
      children: [
        Text('$counter', style: Theme.of(context).textTheme.displayMedium),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton(
                onPressed: () => ref.read(counterProvider.notifier).increment(),
                icon: const Icon(Icons.add)),
            IconButton(
                onPressed: () => ref.read(counterProvider.notifier).decrement(),
                icon: const Icon(Icons.remove)),
          ],
        ),
      ],
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo 2',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: ProviderScope(
        child: MaterialApp(
          home: Scaffold(
            appBar: AppBar(title: const Text('BloC and Riverpod Example 2')),
            body: Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 8),
                  child: Text('BloC Counter',
                      style: Theme.of(context).textTheme.displaySmall),
                ),
                BlocCounterExample(),
                Container(
                  padding: const EdgeInsets.only(top: 8),
                  child: Text('Riverpod Counter',
                      style: Theme.of(context).textTheme.displaySmall),
                ),
                RiverpodCounterExample(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
