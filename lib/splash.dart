import 'package:flutter/material.dart';
import 'package:flutter_example/login_screen.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;
  bool _isAnimationTranslateEnd = false;

  @override
  void initState() {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 4000), vsync: this);
    _animation =
        Tween<double>(begin: 1.0, end: 0.0).animate(_animationController);

    super.initState();
    setState(() {
      _isAnimationTranslateEnd = true;
    });
  }

  void navigateLogin() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => const LoginPage(
          title: '',
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TickerFuture ticketFuture = _animationController.forward();
    ticketFuture.whenCompleteOrCancel(() {
      navigateLogin();
    });
    return Scaffold(
      body: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            flex: 1,
            child: FadeTransition(
                opacity: _animation,
                child: Container(
                  color: Colors.blue,
                  constraints: const BoxConstraints.expand(),
                  child: Center(
                    child: Stack(
                      children: [
                        AnimatedPositioned(
                          left: _isAnimationTranslateEnd ? 100 : -100,
                          top: 0,
                          bottom: 0,
                          duration: const Duration(milliseconds: 2000),
                          child: const Text('Flutter'),
                        )
                      ],
                    ),
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
