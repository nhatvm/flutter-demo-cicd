class A {
  A() {
    print('hehe');
  }
}

class B extends A {
  B(): super();
  _go() {
    print('huhu');
  }
}

void main() {
  var b = B();
  print(b._go());
}
